import { Component, OnInit } from '@angular/core';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-c',
  templateUrl: './c.component.html',
  styleUrls: ['./c.component.css']
})
export class CComponent implements OnInit {

  Nombre = '';
  Tipo = '';
  Curso = '';

  constructor() { }

  ngOnInit(): void {
  }
  constancia() {
    const doc = new jsPDF({orientation: 'l'});
    const img = './assets/ITC.png';
    const img2 = './assets/TecNM.png';
    const firma1 = './assets/Firma1.png';
    const firma2 = './assets/Firma2.png';

    doc.addImage(img, 'png', 10, 10, 40, 40)
    doc.addImage(img2, 'png', 225, 10, 70, 30)

    doc.text('se le otorga la presente', 130, 70)
    doc.text('CONSTANCIA', 140, 80)
    doc.text('a:', 150, 90)
    doc.text(this.Nombre, 120, 100);
    doc.text('Por haber concluido satisfactoriamente', 100, 120)
    doc.text('EL CURSO :', 120, 130);
    doc.text(this.Tipo, 160, 130);
    doc.text(this.Curso, 130, 140);
    
    doc.addImage(firma1, 'png', 40, 150, 40, 40)
    doc.text('Ing. Morales Huertos Ismael', 20, 195)
    doc.text('Titular del curso', 30, 205)
    doc.addImage(firma2, 'png', 225, 150, 40, 40)
    doc.text('Ing. Morales Huertos Ismael', 225, 195)
    doc.text('Director del Mundo', 235, 205)

    window.open(doc.output('bloburl'));
  }
}
