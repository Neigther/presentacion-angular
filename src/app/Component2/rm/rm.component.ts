import { Component, OnInit } from '@angular/core';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-rm',
  templateUrl: './rm.component.html',
  styleUrls: ['./rm.component.css']
})
export class RMComponent implements OnInit {

  array1 = [1, 2, 3, 4, 5, 6, 7, 8, 9 ,10];
  marcas = ["Nissan", "Ferrari", "Musgtan", "Chevrolet"];
  nombre = ["Juan", "Leyva", "Pablo", "Jimena", "Alexander", "Diego"];
  mix = ["Mix1", 2, 90, "Durango", "rkjhj"];
  obj = [{
    nombre: "juan",
    apellido: "Monte",
    edad: 15,
    signo: "Piscis",
  },{
    nombre: "leo",
    apellido: "castillo",
    edad: 90,
    signo: "tauro",
  }];
  edad = 18;
  color = "verde";
  color2 = "violeta";
  pregunta = "acepto";


  Paciente = '';
  Edad = '';
  Alergias = '';
  Talla = '';
  _IMC = '';
  PArterial = '';
  Sexo = '';
  Peso = '';
  Temp = '';
  Diagnostico = '';
  Medicamento = '';

  constructor() { }

  ngOnInit(): void {
  }
  RMedica(){
    const doc = new jsPDF({orientation: 'l'});
    const img = './assets/Doctor.png';
    const img2 = './assets/Doctor.png';
    const firma2 = './assets/Firma2.png';

    doc.addImage(img, 'png', 10, 10, 40, 40)
    doc.addImage(img2, 'png', 245, 10, 40, 40)

    doc.text('Consultorio el Kriko', 130, 30)
    doc.text('DR. Chapatin Criko Marin, prof 0000 0000, Universidad Mexicana ', 80, 40);
    doc.text('Alámo No. 123, Col. Valles, Mexico, Ags. Tel: 666 ', 90, 50);
    doc.text('Nombre del Paciente: ', 20, 70);
    doc.text(this.Paciente, 90, 70);

    doc.text('Edad:', 20, 80);
    doc.text(this.Edad, 36, 80);
    doc.text('años', 45, 80);
    doc.text('Sexo:', 65, 80);
    doc.text(this.Sexo, 82, 80);
    doc.text('Alergias: ', 115, 80);
    doc.text(this.Alergias, 145, 80);
    doc.text('Talla: ', 20, 90);

    doc.text(this.Talla, 35, 90);
    doc.text('Peso: ', 55, 90);
    doc.text(this.Peso, 70, 90);
    doc.text('IMC: ', 90, 90);
    doc.text(this._IMC, 110, 90);
    doc.text('Temperatura: ', 136, 90);
    doc.text(this.Temp, 173, 90);
    doc.text('Presión Alterial: ', 195, 90);
    doc.text(this.PArterial, 235, 90);
    
    doc.text('Diagnóstico: ', 20, 100);
    doc.text(this.Diagnostico, 55, 100);
    doc.text('Tratamiento: ', 20, 110);
    doc.text(this.Medicamento, 20, 120);

    doc.addImage(firma2, 'png', 130, 150, 40, 40)
    doc.text('Firma del Medico', 130, 195)

    window.open(doc.output('bloburl'));
  }
}
