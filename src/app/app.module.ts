import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { PComponent } from './Potada/p/p.component';
import { CComponent } from './Component/c/c.component';
import { RMComponent } from './Component2/rm/rm.component';
import { PerfilComponent } from './Paginademi/perfil/perfil.component';
import { InformacionComponent } from './Paginademi/informacion/informacion.component';
import { RedessocialesComponent } from './Paginademi/redessociales/redessociales.component';

@NgModule({
  declarations: [
    AppComponent,
    PComponent,
    CComponent,
    RMComponent,
    PerfilComponent,
    InformacionComponent,
    RedessocialesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }