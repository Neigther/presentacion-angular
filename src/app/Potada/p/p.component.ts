import { Component, OnInit } from '@angular/core';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-p',
  templateUrl: './p.component.html',
  styleUrls: ['./p.component.css']
})
export class PComponent implements OnInit {
  
  constructor() {}

  ngOnInit(): void {}
  print(){
    const doc = new jsPDF();
    const img = './assets/ITC.png';
    const img2 = './assets/TecNM.png';

    doc.addImage(img, 'png', 10, 10, 40, 40)
    doc.addImage(img2, 'png', 135, 10, 70, 30)

    doc.text('Instituto Tecnologico Nacional de Mexico / Tecnologico de Cuautla', 30, 90);
    doc.text('Programación WEB', 90, 110);
    doc.text('Morales Huertos Ismael 19680202 7° Semestre', 50, 120);
    doc.text('Grupo 3', 100, 130);
    doc.text('Docente: Urzúa Sanchez Guillermo', 70, 140);
    doc.text('Unidad 2: No se como se llama', 70, 150);
    doc.text('Fecha de Entrega: 28-Septiembre-2022', 90, 280);

    window.open(doc.output('bloburl'));
    // doc.output("dataurlnewwindow",{filename: 'por.pdf'});

    // doc.save('Portada.pdf')
  }
  }